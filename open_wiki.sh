#!/usr/bin/expect

spawn ssh -N -L 10000:grav:80 grav@198.58.118.73
expect {
  # Agree to modify known_hosts if prompted
  "(yes/no)?"   {
      send -- "yes\r"
      exp_continue
  }

  # Enter the password
  "*?assword:*" {
     send -- "pAeKUR2oc/PMc4njcRcL\r"
  }
}
sleep 1
system open -a "Safari" http://localhost:10000

interact
