# Tupl Ramp-up

## How to access the wiki

Save the snippet below as `open_wiki.sh` and make it executable.

```
#!/usr/bin/expect

spawn ssh -N -L 10000:grav:80 grav@198.58.118.73
expect {
  # Agree to modify known_hosts if prompted
  "(yes/no)?"   {
      send -- "yes\r"
      exp_continue
  }

  # Enter the password
  "*?assword:*" {
     send -- "pAeKUR2oc/PMc4njcRcL\r"
  }
}
sleep 1
system open -a "Safari" http://localhost:10000

interact
```


## Useful Wiki links

Site  | Url
--|--
Wiki  | http://localhost:10000/index
Git (Bitbucket)  | https://bitbucket.org/tupl/
Jenkins | http://198.58.118.73:8080


## Tune your terminal with oh my zsh

From the oh my zsh [website](https://github.com/robbyrussell/oh-my-zsh):

```
Oh My Zsh is a way of life!

Once installed, your terminal shell will become the talk of the town or your money back! With each keystroke in your command prompt, you'll take advantage of the hundreds of powerful plugins and beautiful themes. Strangers will come up to you in cafés and ask you, "that is amazing! are you some sort of genius?"
```

### Aliases

Edit your zshconfig located in `~/.zshrc` and add these to the bottom of the file.

```
alias zshconfig="atom ~/.zshrc"
alias ohmyzsh="atom ~/.oh-my-zsh"
alias dc="docker-compose"
alias dm="docker-machine"
```


### Oh my zsh plugins

There are tons of pluings for this tool. These are the ones that I currently use:

```
plugins=(git git-remote-branch gitignore httpie python brew npm docker docker-compose osx sbt scala)
```


## Setup your computer

### Maven configuration

Follow the instructions in the wiki http://localhost:10000/development-environment

This guidelines apply only to Mac users.

### 1. Install Homebrew

Homebrew is a package manager for Mac. Similar to `yum` or `apt` for linux.

Installation instructions can be found here: http://brew.sh/

### 2. Install Java 8

`brew cask install java`

### 3. Install scala

`brew install scala`

### 4. Install git

`brew install git`

### 5. Install Maven

`brew install maven`

### 6. Install sbt

`brew install sbt`

### 7. Install Spark

`brew install apache-spark`


### Install other tools

App | command
-----|-------
Sococo | `brew cask install Caskroom/cask/sococo`
Tunnelblick VPN | `brew install Caskroom/cask/tunnelblick`
Slack | `Caskroom/cask/slack`
