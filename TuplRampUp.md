# Tupl Ramp-up

## How to access the wiki

Save the snippet below as `open_wiki.sh` and make it executable.

```
#!/usr/bin/expect

spawn ssh -N -L 10000:172.17.0.2:80 grav@198.58.118.73
expect {
  # Agree to modify known_hosts if prompted
  "(yes/no)?"   {
      send -- "yes\r"
      exp_continue
  }

  # Enter the password
  "*?assword:*" {
     send -- "pAeKUR2oc/PMc4njcRcL\r"
  }
}
sleep 1
system open -a "Safari" http://localhost:10000

interact
```
